export { isObject }
from "./isObject";
export { isNumber }
from "./isNumber";
export { isArray }
from "./isArray";