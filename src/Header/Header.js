import './Header.scss';

class Header {
    constructor() {
        this.header = document.createElement('div');
        const header = this.header;
        
        header.className = 'header';

        return this.header;
    }
}

export default Header;
