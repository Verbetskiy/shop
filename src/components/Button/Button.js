import { Component } from "../../core/Component";
import cn from "classnames";
import "./Button.scss";

const DEFAULT_TYPE = "button";

export class Button extends Component {
    constructor(options = {}) {
        const {
            type = DEFAULT_TYPE,
                className,
                children,
                onClick,
                ...other
        } = options;

        super({
            tagName: "button",
            className: `${className}`,
            attrs: { type, ...other },
            listeners: {},
            children,
        });
    }
}