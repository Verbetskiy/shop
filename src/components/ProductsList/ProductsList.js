import { Component } from "../../core/Component";
import { Product } from "./Product/Product";
import { Button } from "../Button/Button";
import "./ProductsList.scss";

import JSON_PRODUCTS from "../assets/database/products.json";

const products = JSON_PRODUCTS.map((el) => {
    const {
        id,
        category,
        model,
        manufacturer,
        country,
        imageSrc,
        price,
        rating,
        description,
        warranty,
    } = el;

    const btn = new Button({ children: [`Cart`], className: "product__btn" });
    const domElem = `
      <div class="main__content-wrapper">
        <div class="main__content-manufacturer">${manufacturer}</div>
        <div class="main__content-category">${category}</div>
        <div class="main__content-img__wrapper">
          <img class="main__content-img" src=${imageSrc}></img>
        </div>
        <div class="main__content-country">${country}</div>
        <div class="main__content-desc">${description.slice(0, 270)}</div>
        ${btn.toHTML()}
      </div>
     `;

    return domElem;
});

export class ProductsList extends Component {
    constructor() {
        super({
            tagName: "main",
            className: "main",
            children: products,
        });
    }
}