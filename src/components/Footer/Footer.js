import { Component } from '../../core/Component';

import "./Footer.scss";

export class Footer extends Component {
  constructor() {
    super({
      tagName: "footer",
      className: "footer",
    });
  }
}
