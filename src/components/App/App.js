import { ProductsList } from '../ProductsList/ProductsList';
import { Header } from '../Header/Header';
import { Footer } from '../Footer/Footer';
import { Component } from '../../core/Component';
import { Sidebar } from '../Sidebar/Sidebar';

import './App.scss';

export class App extends Component {
    constructor() {
        const sidebar = new Sidebar();
        const main = new ProductsList();

        super({
            className: 'app',
            children: [
                new Header(),
                `<div class="app__content">
                  ${sidebar.toHTML()}
                  <div class="main">
                  ${main.toHTML()}
                  </div>
                </div>`,
                new Footer()
            ]
        });
    }
}
