import { Component } from "../../core/Component";
import { Button } from "../Button/Button";
import { Input } from "../Input/Input";

import "./Header.scss";

const input = new Input();
const search_btn = new Button({
    children: [`Search`],
    className: "header__btn_search",
});

const header__input_wrapper = `
<div class="header__input_wrapper">
    ${input.toHTML()}
    ${search_btn.toHTML()}
</div>
`;

export class Header extends Component {
    constructor() {
        super({
            tagName: "header",
            className: "header",
            children: [
                header__input_wrapper,
                new Button({ children: [`Log Out`], className: "header__btn_logOut" }),
            ],
        });
    }
}