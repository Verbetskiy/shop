import Products from './Products/Products';
import Header from './Header/Header';


import JSON_PRODUCTS from '../products.json';


class App {
    constructor() {
        this.app = document.createElement("div");
        const app = this.app;

        app.id = "app";

        this.appendAllElemnts(app, new Header(), new Products(JSON_PRODUCTS))



        return app;
    }

    appendAllElemnts(app, ...someStuff) {
        if (Array.isArray(someStuff)) {
            app.append(...someStuff)
        } else {
            app.append(someStuff)
        }
    }

}


export default App;