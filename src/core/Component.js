import { isNumber } from "../utils/index";

const DEFAULT_TAG_NAME = "div";

export class Component {
    constructor(options) {
        const {
            tagName = DEFAULT_TAG_NAME,
                className,
                attrs,
                children,
                listeners,
        } = options;

        this._component = document.createElement(tagName);
        this._component.className = className;

        for (let attrName in attrs) {
            const attrValue = attrs[attrName];

            if (!attrValue && !isNumber(attrValue)) continue;

            this._component.setAttribute(attrName, attrValue);
        }

        if (!Array.isArray(children)) return;

        for (const eventType in listeners) {
            const handler = listeners[eventType];

            if (!handler) continue;

            this._component.addEventListener(eventType, handler);
        }

        for (let child of children) {
            const isHTML = typeof child === "string";

            if (isHTML) {
                this._component.innerHTML += child;
            } else {
                this._component.append(child.toNode());
            }
        }
    }
    toNode() {
        return this._component;
    }
    toHTML() {
        return this._component.outerHTML;
    }
}