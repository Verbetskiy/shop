import { App } from "./components/App/App";
import { render } from "./core/render";

import "./style.scss";
const root = document.getElementById("root");

render(new App(), root);