import Product from './Product/Product';


class Products {
    constructor(goods) {
        this.products = document.createElement("div");
        const products = this.products;

        products.className = "products";

        const parsedGoods = goods.map((product) => {

            return new Product(product.category, product.country, product.description, product.imageSrc, product.id)
        });

        products.append(...parsedGoods)


        return this.products;
    }
}

export default Products;