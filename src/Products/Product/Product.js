import Button from "./Button/Button"

class Product {
    constructor(category, country, description, imageSrc, id) {
        this.product = document.createElement('div');

        const product = this.product;
        product.className = "product";
        product.id = id;

        const btn = new Button(product.className, "Buy");

        product.className = 'product';
        product.innerHTML = `
        <img class=product_img src=${imageSrc}>
        <h1>${category}</h1>
        <h1><u>${country}</u></h1>
        <h1>${description.slice(0, 250)}...</h1>
        `
        product.append(btn);

        btn.addEventListener("click", (e) => e.target.parentNode.remove());

        return product;
    }

}

export default Product;