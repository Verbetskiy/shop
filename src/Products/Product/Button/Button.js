class Button {
    constructor(className, text) {
        this.button = document.createElement("button");
        const button = this.button;



        button.className = `${className}_btn`;
        button.textContent = `${text}`;

        return button;
    }



}

export default Button;